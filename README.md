# Embedded Annotations, Online Appendix #

This online appendix contains supplementary material for our submission 

## Study of Clafer Web Tools (CWT) ##

###Project Overview###
We simulated the development history of Clafer Web Tools by retroactively adding feature annotations to the development history. All the history before 0.3.5 release has been simulated. This project aims to study the cost and benefit of our proposed eager annotation approach, and also facilitate future action research and tool development.

###Repositories###
The 4 simulation branches are available as forked repositories of the original Clafer Web Tools repositories at https://github.com/redmagic4 In the repositories, the branches named "reenactPilot2" are the simulation branches, and commits made by "Wenbin Ji w6ji@gsd.uwaterloo.ca" are simulation commits. Commits not made by "Wenbin Ji w6ji@gsd.uwaterloo.ca" are original commits. (Branch "reenactPilot1" is our earlier pilot study, which is not of interest here.)

Link to the simulated dataset Configurator variant: https://bitbucket.org/easelab/datasetclaferconfigurator/src/master/

Link to the simulated dataset IDE variant: https://bitbucket.org/easelab/datasetclaferide/src/master/

Link to the simulated dataset Visualizer variant: https://bitbucket.org/easelab/datasetclafermoovisualizer/src/master/

Link to the simulated dataset Platform variant: https://bitbucket.org/easelab/datasetclafertoolsuicommonplatform/src/master/

###Data Spreadsheets###
We present the data of cost and reuse benefit in the Excel sheet cwt_detailed.xlsx in this online appendix. There are two sheets in the file, "Periods" and "Feature Propagations."

In the sheet "Periods," Row 1 ~ Row 211 present information of all the 210 periods in the simulation study (a period is a set of original commits that were merged into the simulation branch together in one merging commit). In each row, columns A-C record the project and the index of the period (index is in chronological order). Columns D-F record the evolution patterns the period involves. Column G records the number of original commits in the period. Column H records the merging commit of the period in the simulation branch. Columns I-Y record the cost involved in each period (measured in number of annotation markers added/deleted/modified, separated to different columns according to different patterns). Column Z records some comments, including important milestones (releases, start points of transiting into platform) and a few descriptions about interesting periods. Column AA records the features whose annotations were added or changed in the periods. 

Line 212 of sheet "Periods" sums up the cost of each pattern and the total cost. Line 215-231 present the frequency of patterns (number of periods involving a certain pattern). In the paper, we present the frequency in all the simulated history. Here, we also present the frequency in each subject projects respectively.

In sheet "Feature propagations", Row 1~Row 56 present all the 55 feature propagations found in the study. Column A records the index (in chronological order). Column B-C record the source and target project of the propagation. Column D records the type of the propagation. Column E records the propagated feature. Column F-G record the source and target commits. Column H describes the annotations of the feature when it was propagated. Colume I-J record data about the accuracy of the annotations (presented in the paper to answer RQ4). Columns M, N, P record the evolution history and cost of the propagated feature (evolution activities happened to the annotations of the feature before it was propagated, and the cost involved in those activities). Columns O records the number of annotations that provided benefit, which is used in the paper to answer RQ3.

Row 59-61 of sheet "Feature propagations" count the number of propagations where file/folder/fragment annotations were involved, which is used for analyzing granularity of annotations. Row 63-64 present the number of cases where only one feature was propagated in a commit vs. several features were propagated together in one commit.

## Study of a Commercial Firmware (FW) Project ##

This study followed the same methodology. The spreadsheet (named fw_detailed.xlsx) in this online appendix is structured the same way as that of CWT. However, the original feature names have been anonymized.

## Study of a Bitcoin Wallet App (BC) ##

This study followed the same methodology. The spreadsheet (named bc_detailed.xlsx) in this online appendix is structured the same way as that of CWT. A link to the simulated datasets is found here https://bitbucket.org/easelab/datasetbitcoinwallet-simulation/src/master/